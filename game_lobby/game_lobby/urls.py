""" Default urlconf for game_lobby """

from django.conf.urls import include, patterns, url
from django.contrib.auth.decorators import login_required
from django.contrib import admin

from game_lobby.views import LobbyView, LobbyConnecterView
admin.autodiscover()

urlpatterns = patterns('game_lobby.views',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', 'index', name='index'),
    url(r'^login/', 'ajax_login', name='ajax_login'),
    url(r'^registration/', 'ajax_registration', name='ajax_registration'),
    url(r'^logout/', 'logout_view', name='logout_view'),

    url(r'^getdata/', 'getdata', name='getdata'),
    url(r'^updated/', 'updated', name='updated'),

    url(r'^lobby/', login_required(LobbyView.as_view()), name='lobby'),
    url(r'^lobbycon/', login_required(LobbyConnecterView.as_view()), name='lobbycon'),)
