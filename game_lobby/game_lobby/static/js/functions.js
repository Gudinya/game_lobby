var owned_lobby_id = '';

function setGameStatus(status){
  $('.gamestatus').hide();
  $('#gms_' + status).show();
}

function start_game_btn_enabled(value) {
  var startgame = $('#startgame');
  if (value) {
    startgame.removeClass('disabled');
    startgame.text($('#tr_start').text());
  } else {
    startgame.text($('#tr_wait').text());
    startgame.removeClass('disabled');
    startgame.addClass('disabled');
  }
}

function leftgame(lobbyid){
  $.ajax({
    type: "DELETE",
    url: "/lobbycon/",
    data: {'lobby_id': lobbyid},
    success: function(data) {
      $("#modal-play").find('.text-error').hide();
      $('#modal-play').removeClass('md-show');
      // if (data.error){
      //   setGameStatus('');
      //   $("#modal-play").find('.text-error').text(data.error);
      //   $("#modal-play").find('.text-error').show();
      // } else {
      //   $("#modal-play").find('.text-error').hide();
      //   $('#modal-play').removeClass('md-show');
      // }
    }
  });
}

function confirmjoin(){
  lobbyid = $(this).data('lobbyid');
  $('#modal-join').addClass('md-show');
  $('#modal-join .btnyes').data('lobbyid', lobbyid);
}

function joingame() {
  lobbyid = $(this).data('lobbyid');

  $('#modal-play').addClass('md-show');
  $('#modal-play').data('lobbyid', lobbyid);

  if (owned_lobby_id == lobbyid) {
    setGameStatus('start');
  } else {
    setGameStatus('wait');
    $.ajax({
      type: "POST",
      url: "/lobbycon/",
      data: {'lobby_id': lobbyid},
      success: function(data) {
        if (data.error){
          setGameStatus('');
          $("#modal-play").find('.text-error').text(data.error);
          $("#modal-play").find('.text-error').show();
        } else {
          $("#modal-play").find('.text-error').hide();
        }
      }
    });
  }

  $('#modal-play .md-close').click(function(){
    leftgame(lobbyid);
  });
}

var prev_data = null,           // remember data fetched last time
    waiting_for_update = false, // are we currently waiting?
    LONG_POLL_DURATION = 60000; // how long should we wait? (msec)

function load_data() {

  var url = '/getdata'

  $.ajax({ url: url,
    success: function(data) {
        display_data(data);
        wait_for_update();
    },
  });
  return true;
}

function wait_for_update() {
  if (!waiting_for_update) {

    waiting_for_update = true;

    $.ajax({ url: '/updated',
      success: load_data,
      error: function(jqXHR, textStatus, errorThrown) {
        setTimeout(function() {
          waiting_for_update = false;
          wait_for_update();
        }, 5000);
      },
      success: function (data) {
        load_data(data)
        waiting_for_update = false;
        wait_for_update();
      },
      timeout:  LONG_POLL_DURATION,
    });
  }
}

function display_data(data) {
  if (data && (data != prev_data)) {

    prev_data = data;

    $('.twousr').remove();
    $('.fourusr').remove();
    $('.addedroom').remove();
    $('.jstusr').remove();

    var two_cnt = 0,
        four_cnt = 0;

    var opened_lobby_id = $('#modal-play').data('lobbyid'),
        close_game = true;

    owned_lobby_id = data.owned_lobby_id;

    var lobbys = data.lobbys;
    for (var i = 0; i < lobbys.length; i++) {

      var lobbyusers = lobbys[i].lobby_conn;

      if (lobbys[i].lobby_type == 1){
        two_cnt += 1
      } else {
        four_cnt += 1
      }

      var lobby_user_cnt = 0;

      var lobbyrow = $('#cproom').clone();

      var lobbyid = lobbys[i].lobby_id;

      // if lobby is closed we have to close opened window
      if (lobbys[i].lobby_id == opened_lobby_id) close_game = false;

      lobbyrow.data('lobbyid', lobbyid);
      lobbyrow.on('click', confirmjoin);

      lobbyrow.addClass('addedroom');
      lobbyrow.removeAttr('id');
      lobbyrow.find('h3').text(lobbys[i].lobby_title);

      if (lobbys[i].lb_started !== null && opened_lobby_id == lobbyid){
        setGameStatus('playing');
      }

      for (var k = 0; k < lobbyusers.length; k++) {
        var usrrow = $('#cpusr').clone();
        usrrow.removeAttr('id');
        usrrow.find('.span12').text(lobbyusers[k].username)

        lobbyrow.find('.lbusers').append(usrrow.clone());

        lobby_user_cnt += 1;

        // show usernames over grouped lobbys
        if (lobbys[i].lobby_type == 1){
          usrrow.addClass('twousr');
          $('#two_users').append(usrrow);
        } else {
          usrrow.addClass('fourusr');
          $('#four_users').append(usrrow);
        }
      } // users loop

      // lock lobby if max of users connected
      lr_ico = lobbyrow.find('.fa');

      if ((lobbys[i].lobby_type == 1 && lobby_user_cnt == 2) || (lobbys[i].lobby_type == 2 && lobby_user_cnt == 4)){
        lobbyrow.addClass('closed-room');

        lr_ico.removeClass('fa-users');
        lr_ico.addClass('fa-lock');
        lobbyrow.off('click');

        lobbyrow.find('.lbstatus').css('color', 'white').text($('#st_wait_start').text());

        if (opened_lobby_id == lobbyid) {
          if ($('#gms_start').is(':hidden')) {
            setGameStatus('wait_start');
          }
          start_game_btn_enabled(true);
        }

      }
      else {
        lobbyrow.removeClass('closed-room');
        lr_ico.addClass('fa-users');
        lr_ico.removeClass('fa-lock');

        lobbyrow.find('.lbstatus').css('color', 'green').text($('#st_wait').text());

        if (opened_lobby_id == lobbyid) {
          if ($('#gms_start').is(':hidden')) {
            setGameStatus('wait');
          }
          start_game_btn_enabled(false);
        }
      }

      if (lobbys[i].lb_started !== null){
        lobbyrow.addClass('closed-room');

        lr_ico.removeClass('fa-users');
        lr_ico.addClass('fa-lock');
        lobbyrow.off('click');

        lobbyrow.find('.lbstatus').css('color', 'red').text($('#st_playing').text());
        if (opened_lobby_id == lobbyid) {
          setGameStatus('playing');
        }
      }

      // connected to lobby users count
      lobbyrow.find('.concnt').text(lobby_user_cnt);

      // adding lobby to list of abvalible lobbys
      if (lobbys[i].lobby_type == 1){
        $('#tworooms').append(lobbyrow);
      } else {
        $('#fourrooms').append(lobbyrow);
      }

    } // lobby loop

    // close opened lobby
    if (close_game){
      $("#modal-play").find('.text-error').hide();
      $('#modal-play').removeClass('md-show');
    }

    $('#twocnt').text(two_cnt);
    $('#fourcnt').text(four_cnt);

    var just_connected = data.just_connected;

    for (var i = 0; i < just_connected.length; i++) {
      var usrrow = $('#cpusr').clone();
      usrrow.removeAttr('id');
      usrrow.find('.span12').text(just_connected[i].username);
      usrrow.addClass('jstusr');

      $('#free_users').append(usrrow);
    }

  }
}

$(document).ready(function () {

  function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
  }
  var csrftoken = getCookie('csrftoken');

  function csrfSafeMethod(method) {
  // these HTTP methods do not require CSRF protection
  return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
  }
  function sameOrigin(url) {
    // test that a given url is a same-origin URL
    // url could be relative or scheme relative or absolute
    var host = document.location.host; // host + port
    var protocol = document.location.protocol;
    var sr_origin = '//' + host;
    var origin = protocol + sr_origin;
    // Allow absolute or scheme relative URLs to same origin
    return (url == origin || url.slice(0, origin.length + 1) == origin + '/') ||
        (url == sr_origin || url.slice(0, sr_origin.length + 1) == sr_origin + '/') ||
        // or any other URL that isn't scheme relative or absolute i.e relative.
        !(/^(\/\/|http:|https:).*/.test(url));
  }
  $.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && sameOrigin(settings.url)) {
            // Send the token to same-origin, relative URLs only.
            // Send the token only if the method warrants CSRF protection
            // Using the CSRFToken value acquired earlier
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
  });

  /* --- Open Section --- */
  $(".section").click(function () {
    $(this).addClass("section-expand");
    $(".section-close").show(250);
    $('.userlist').hide();
  })

  /* --- Close Section --- */
  $(".section-close").click(function () {
    $(".section").removeClass("section-expand");
    $(".section-close").hide(250);
    $(".section-fortwo i, .section-forfour i").removeClass("active");
    $('.userlist').show();
  })


  /* --- Switch Section --- */
  $(".section-fortwo").click(function () {
    $(".section").removeClass("section-expand");
    $("#fortwo").addClass("section-expand");
  })
  $("#fortwo").click(function () {
    $(".section-fortwo i").toggleClass("active");
  })

  $(".section-forfour").click(function () {
    $(".section").removeClass("section-expand");
    $("#forfour").addClass("section-expand");
  })
  $("#forfour").click(function () {
    $(".section-forfour i").toggleClass("active");
  })

  /* --- Item Description --- */
  $(".item").click(function () {
    $(this).toggleClass("open");
  })


  /* --- Swap color --- */
  $('.change-white, .change-black, .change-multi').click(function () {
    $('#swap-color').attr('href','css/color/'+$(this).data('color')+'.css');
    return false;
  });


  $("#login").submit(function(event){
    event.preventDefault();

    $.ajax({
      type: "POST",
      url: "/login/",
      data: $(this).serialize(),
      success: function(data) {
        if (data.error) {
          $('#login .text-error').show();
          $('#login .text-error').text(data.error);
        } else {
          location.reload();
        }
      }
     });

    return false;
  });

  $('#shregform').click(function(){
    $('.loginform').hide();
    $('.regform').show();
  });

  $("#register").submit(function(event){
    event.preventDefault();

    $.ajax({
      type: "POST",
      url: "/registration/",
      data: $(this).serialize(),
      success: function(data) {
        if (data.error) {
          $('#register .text-error').show();
          $('#register .text-error').text(data.error);
        } else {
          location.reload();
        }
      }
     });

    return false;
  });

  $('#lbadd').submit(function(event){
    event.preventDefault();

    $.ajax({
      type: "POST",
      url: "/lobby/",
      data: $(this).serialize(),
      success: function(data) {
        if (data.error){
          $('#lbadd').find('.text-error').text(data.error);
          $('#lbadd').find('.text-error').show();
        } else {
          owned_lobby_id = data.lobby_id;
          $('#modal-play').data('lobbyid', data.lobby_id);

          setGameStatus('start');
          start_game_btn_enabled(false);

          $('#modal-add').removeClass('md-show');
          $('#modal-play').addClass('md-show');
          $('#modal-play .md-close').click(function(){
            leftgame(owned_lobby_id);
          });
        }
      }
     });
    return false;
  });

  $('#addlbtwo').click(function (){
    $('#lbtype').val('1');
    $('#lbadd').find('.text-error').hide();
  });

  $('#addlbfour').click(function (){
    $('#lbtype').val('2');
    $('#lbadd').find('.text-error').hide();
  });

  $('#startgame').click(function(){
    if ($(this).hasClass('disabled')) return;
    var modalplay = $('#modal-play');
    $.ajax({
      type: "PATCH",
      url: "/lobby/",
      data: {'lobby_id': modalplay.data('lobbyid')},
      success: function(data) {
        if (data.error){
          modalplay.find('.text-error').text(data.error);
          modalplay.find('.text-error').show();
        } else {
          modalplay.find('.text-error').hide();
          setGameStatus('playing');
        }
      }
     });
  });

  // confirm window actions
  $('#modal-join .btnno').click(function(){$('#modal-join').removeClass('md-show')});
  var joinyes = $('#modal-join .btnyes');

  joinyes.click(function(){
    $('#modal-join').removeClass('md-show');
  });
  joinyes.click(joingame);


  // singout
  $('.section-signout').click(function(){
    $.ajax({
      type: "GET",
      url: "/logout/",
      success: function(data) {
        location.reload();
      }
     });
  })
});

