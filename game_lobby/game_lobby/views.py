import time
import threading

import simplejson
import datetime
from urlparse import parse_qs

from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.db import IntegrityError
from django.utils.translation import ugettext as _
from django.views.generic import View

from django.core.cache import cache

from .models import UsersOnline, LobbyConnected, Lobby


CLOSE_LOBBY_AFTER = 60


def close_lobby(lb_obj):
    lb_obj.delete()
    somthing_changed()


def somthing_changed():
    last_change_num = cache.get('last_change_num', 0) + 1
    cache.set('last_change_num', last_change_num)


def set_user_online(user):
    try:
        uo = UsersOnline.objects.get(uo_user=user)
    except UsersOnline.DoesNotExist:
        uo = UsersOnline(uo_user=user, uo_checked=datetime.datetime.now())

    # clear info about disconnected users
    filter_time = datetime.datetime.now() - datetime.timedelta(seconds=90)
    UsersOnline.objects.filter(uo_checked__lte=filter_time).delete()

    uo.uo_checked = datetime.datetime.now()
    uo.save()


def error_response(msg):
    return HttpResponse(simplejson.dumps({'error': msg}),
                        content_type="application/json")


def index(request):
    """ Default view for the root """
    return render(request, 'index.html')


def updated(request):
    last_change_num = cache.get('last_change_num', 0)
    # will be a problem if user refresh page too many times
    for x in xrange(1, 30):
        if last_change_num != cache.get('last_change_num', 0):
            break
        time.sleep(2)

    return HttpResponse(simplejson.dumps({'updated': True}),
                        content_type="application/json")


def logout_view(request):
    logout(request)
    return redirect(index)


def ajax_login(request):
    """
    This view logs a user in using the POST data.
    """
    data = {}
    if request.method == 'POST':

        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)

        if (user is not None) and (user.is_active):
            login(request, user)
            request.session.set_expiry(0)
        else:
            data['error'] = _('There was an error logging you in. Please Try again.')
    return HttpResponse(simplejson.dumps(data), content_type="application/json")


def ajax_registration(request):
    """
    This view reg a user in using the POST data.
    """
    data = {}
    if request.method == 'POST':

        username = request.POST['username']
        password = request.POST['password']

        try:
            User.objects.create_user(username, password=password)
            user = authenticate(username=username, password=password)
            login(request, user)
            request.session.set_expiry(0)
        except IntegrityError as e:
            if 'UNIQUE' in e.message:
                data['error'] = _('User exists')
            else:
                data['error'] = str(e)
        except Exception as e:
            data['error'] = str(e)

    return HttpResponse(simplejson.dumps(data),
                        content_type="application/json")


def getdata(request):
    data = {'lobbys': [], 'just_connected': [], 'owned_lobby_id': None}

    if not request.user.is_authenticated():
        return HttpResponse(simplejson.dumps(data), content_type="application/json")

    set_user_online(request.user)

    for lb_data in Lobby.objects.select_related():
        started = 1 if lb_data.lb_started else None
        lobby_data = {'lobby_id': lb_data.id,
                      'lobby_title': lb_data.lb_title,
                      'lobby_type': lb_data.lb_type.id,
                      'lobby_owner': lb_data.lb_owner.username,
                      'lb_started': started,
                      'lobby_conn': []}

        for lb_conn in LobbyConnected.objects.select_related().\
                filter(lc_loby_id=lb_data.id):
            lobby_data['lobby_conn'].append({
                'username': lb_conn.lc_user.username})

        data['lobbys'].append(lobby_data)

    try:
        lobby = Lobby.objects.get(lb_owner=request.user)
        data['owned_lobby_id'] = lobby.id
    except Lobby.DoesNotExist:
        pass

    # not a best solution
    uo = UsersOnline.objects.select_related().exclude(
        uo_user_id__in=LobbyConnected.objects.all().values('lc_user_id'))
    for uo_row in uo:
        data['just_connected'].append({'username': uo_row.uo_user.username})

    return HttpResponse(simplejson.dumps(data),
                        content_type="application/json")


class LobbyView(View):

    # create lobby
    def post(self, request, *args, **kwargs):
        lb_title = request.POST.get('lbtitle', None)

        if not lb_title:
            return error_response(_('You have to set a lobby name'))

        lb_type_id = request.POST['lbtype']

        # we have to check, does user has lobby and if he has
        # we have to check numbers of connected users
        # and if he is the only one :) we have to delete this lobby
        # to allow connect to ther other
        try:
            owner_lobby = Lobby.objects.get(lb_owner=request.user)
            if LobbyConnected.objects.filter(lc_loby=owner_lobby).count() < 2:
                owner_lobby.delete()
        except Lobby.DoesNotExist:
            pass

        # we have to check, does user connected to another lobby
        # that has been started
        lcuser_in = LobbyConnected.objects.filter(lc_user=request.user, lc_loby_id__in=Lobby.objects.all().filter(lb_started__isnull=False).values('id'))

        if lcuser_in.count() > 0:
            return error_response(_('You are playing in another lobby'))
        else:
            LobbyConnected.objects.filter(lc_user=request.user).delete()

        try:
            lobby = Lobby(lb_title=lb_title,
                          lb_type_id=lb_type_id,
                          lb_owner=request.user)
            lobby.save()
        except Exception, e:
            if 'UNIQUE' in e.message:
                if 'lb_title' in e.message:
                    return error_response(_('Lobby with such name exists'))
                else:
                    return error_response(_('You have created lobby'))
            else:
                return error_response(str(e))

        lobby_con = LobbyConnected(lc_loby=lobby,
                                   lc_user=request.user)
        lobby_con.save()

        somthing_changed()

        return HttpResponse(simplejson.dumps({'lobby_id': lobby.id}),
                            content_type="application/json")

    # start game
    def patch(self, request, *args, **kwargs):
        try:
            lobby_id = parse_qs(request.body)['lobby_id'][0]
        except:
            return error_response(_('Lobby ID not specified'))

        try:
            lobby = Lobby.objects.get(id=lobby_id)
        except Lobby.DoesNotExist:
            return error_response(_('You are trying to start game in not existed lobby'))

        user_havetoplay = 2 if lobby.lb_type_id == 1 else 4
        user_connected = LobbyConnected.objects.filter(lc_loby=lobby).count()

        if user_havetoplay != user_connected:
            return error_response(_('Lobby not full enough'))

        if (LobbyConnected.objects.
                filter(lc_user=request.user).
                exclude(lc_loby=lobby).count() > 0):
            return error_response(_('You are playing in another lobby'))

        if lobby.lb_owner != request.user:
            return error_response(_('Only owner could start the game'))

        # all checks passed, lets play
        lobby.lb_started = datetime.datetime.now()
        lobby.save()
        threading.Timer(CLOSE_LOBBY_AFTER, close_lobby, [lobby]).start()
        somthing_changed()

        return HttpResponse()


class LobbyConnecterView(View):

    # joingame
    def post(self, request, *args, **kwargs):
        lobby_id = request.POST.get('lobby_id', None)

        if not lobby_id:
            return error_response(_('Lobby ID not specified'))

        try:
            lobby = Lobby.objects.get(id=lobby_id)
        except Lobby.DoesNotExist:
            return error_response(_('You are trying to join to not existed lobby'))

        user_havetoplay = 2 if lobby.lb_type_id == 1 else 4
        user_connected = LobbyConnected.objects.filter(lc_loby=lobby).count()

        # we have to check, does user has lobby and if he has
        # we have to check numbers of connected users
        # and if he is the only one :) we have to delete this lobby
        # to allow create new one
        try:
            owner_lobby = Lobby.objects.get(lb_owner=request.user)
            if LobbyConnected.objects.filter(lc_loby=owner_lobby).count() < 2:
                owner_lobby.delete()
        except Lobby.DoesNotExist:
            pass

        if lobby.lb_started:
            return error_response(_('Sorry, but game is started'))

        if user_havetoplay == user_connected:
            return error_response(_('Lobby is full'))

        if (LobbyConnected.objects.
                filter(lc_user=request.user).
                exclude(lc_loby=lobby).count() > 0):
            return error_response(_('You are playing in another lobby'))

        # all checks passed, lets join
        lobby_con = LobbyConnected(lc_loby=lobby, lc_user=request.user)
        lobby_con.save()
        somthing_changed()

        return HttpResponse()

    # left game
    def delete(self, request, *args, **kwargs):
        try:
            lobby_id = parse_qs(request.body)['lobby_id'][0]
        except:
            return error_response(_('Lobby ID not specified'))

        try:
            lobby = Lobby.objects.get(id=lobby_id)
        except Lobby.DoesNotExist:
            return error_response(_('You are trying to leave not existed lobby'))

        # allow leave not started game
        # otherwise user have to wait
        if lobby.lb_started:
            return error_response(_('You have to wait, until game is finished'))

        try:
            lobby_con = LobbyConnected.objects.get(lc_loby_id=lobby_id, lc_user=request.user)
            lobby_con.delete()
            somthing_changed()
        except LobbyConnected.DoesNotExist:
            pass

        # we have to close all lobby connections when the owner
        # left lobby and game is not started
        if lobby.lb_owner == request.user:
            lobby_con = LobbyConnected.objects.filter(lc_loby=lobby)
            lobby_con.delete()

        # if no one connected to the lobby, we have to delete it
        lobby_con_cnt = LobbyConnected.objects.filter(lc_loby_id=lobby_id).count()

        if lobby_con_cnt == 0:
            lobby.delete()

        return HttpResponse()
