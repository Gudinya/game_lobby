import uuid
import os

from django.db.models.signals import pre_delete
from django.dispatch.dispatcher import receiver
from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _



class LobbyType(models.Model):
    lbt_name = models.CharField(
        max_length=30,
        unique=True,
        verbose_name=_(u'Name'),
        error_messages={'null': _(u'This field cannot be null.'),
                        'blank': _(u'This field cannot be blank.')})

    class Meta:
        verbose_name = _(u'Lobby type')
        verbose_name_plural = _(u'Lobby types')

    def __unicode__(self):
        return self.lbt_name


class Lobby(models.Model):
    lb_title = models.CharField(
        max_length=30,
        unique=True,
        blank=False,
        null=False,
        verbose_name=_(u'Title'),
        error_messages={'null': _(u'This field cannot be null.'),
                        'blank': _(u'This field cannot be blank.')})
    lb_type = models.ForeignKey(
        LobbyType,
        verbose_name=_(u'Type'))
    lb_owner = models.OneToOneField(
        User,
        verbose_name=_(u'Owner'))
    lb_started = models.DateTimeField(
        verbose_name=_(u'Started'),
        blank=True,
        null=True,)

    class Meta:
        verbose_name = _(u'Lobby')
        verbose_name_plural = _(u'Lobbys')

    def __unicode__(self):
        return self.lb_title


class LobbyConnected(models.Model):
    lc_loby = models.ForeignKey(
        Lobby,
        verbose_name=_(u'Lobby'))
    lc_user = models.ForeignKey(
        User,
        unique=True,
        verbose_name=_(u'User'))

    class Meta:
        verbose_name = _(u'Lobby Connects')
        verbose_name_plural = _(u'Lobby Connects')


class UsersOnline(models.Model):
    uo_user = models.ForeignKey(
        User,
        verbose_name=_(u'User'))
    uo_checked = models.DateTimeField(
        verbose_name=_(u'Checked'),)

